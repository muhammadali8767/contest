<?php @session_start();
	ini_set("display_errors","Off");  //set this to "On" for debugging  ,especially when no reason blank shows up.
	//header('X-Frame-Options:SAMEORIGIN');

//for people using hustoj out of China , be careful of the last two line of this file !

// connect db 
static 	$DB_HOST="localhost";  
static 	$DB_NAME="jol";   
static 	$DB_USER="debian-sys-maint";  
static 	$DB_PASS="kBSRWWeQykTSI4XU";
static 	$OJ_NAME="Bosh sahifa";  
static 	$OJ_HOME="./";    
static 	$OJ_ADMIN="root@localhost";  
static 	$OJ_DATA="/home/judge/data"; 
static 	$OJ_BBS=false;//"bbs" for phpBB3 bridge or "discuss" for mini-forum or false for close any 
static  $OJ_ONLINE=false;  
static  $OJ_LANG="en";  
static  $OJ_SIM=false;  
static  $OJ_DICT=false; 
static  $OJ_LANGMASK=0; //1mC 2mCPP 4mPascal 8mJava 16mRuby 32mBash 1008 for security reason to mask all other language
static  $OJ_EDITE_AREA=true;//true: syntax highlighting is active
static  $OJ_ACE_EDITOR=true;
static  $OJ_AUTO_SHARE=false;//true: One can view all AC submit if he/she has ACed it onece.
static  $OJ_CSS="white.css";
static  $OJ_SAE=false; //using sina application engine
static  $OJ_VCODE=false;  
static  $OJ_APPENDCODE=false;  
if(!$OJ_APPENDCODE) 	ini_set("session.cookie_httponly", 1);   
static  $OJ_CE_PENALTY=false;  
static  $OJ_PRINTER=false;  
static  $OJ_MAIL=false; 
static  $OJ_MARK="mark"; // "mark" for right "percent" for WA
static  $OJ_MEMCACHE=false;  
static  $OJ_MEMSERVER="127.0.0.1";
static  $OJ_MEMPORT=11211;
static  $OJ_UDP=true;   
static  $OJ_UDPSERVER="127.0.0.1";
static  $OJ_UDPPORT=1536;
static  $OJ_REDIS=false;   
static  $OJ_REDISSERVER="127.0.0.1";
static  $OJ_REDISPORT=6379;
static  $OJ_REDISQNAME="hustoj";
static  $SAE_STORAGE_ROOT="http://hustoj-web.stor.sinaapp.com/";
static  $OJ_CDN_URL="";  //  http://cdn.hustoj.com/  https://raw.githubusercontent.com/zhblue/hustoj/master/trunk/web/ 
static  $OJ_TEMPLATE="bs3"; // [bs3 ie ace sweet sae] work with discuss3, [classic bs] work with discuss
//if(isset($_GET['tp'])) $OJ_TEMPLATE=$_GET['tp'];
if($OJ_TEMPLATE=="classic") $OJ_CSS="hoj.css";
static  $OJ_LOGIN_MOD="hustoj";
static  $OJ_REGISTER=true; 
static  $OJ_REG_NEED_CONFIRM=false; 
static  $OJ_NEED_LOGIN=false; 
static  $OJ_RANK_LOCK_PERCENT=0; 
static  $OJ_SHOW_DIFF=true; 
static  $OJ_TEST_RUN=false; 
static  $OJ_BLOCKLY=false; 
static  $OJ_ENCODE_SUBMIT=false; 
static  $OJ_OI_1_SOLUTION_ONLY=false; 
static  $OJ_OI_MODE=false;
static  $OJ_SHOW_METAL=true;
static  $OJ_RANK_LOCK_DELAY=3600;//赛后封榜持续时间，单位秒。根据实际情况调整，在闭幕式颁奖结束后设为0即可立即解封。
static  $OJ_BENCHMARK_MODE=false; //此选项将影响代码提交，不再有提交间隔限制，提交后会返回solution id

static  $OJ_NOIP_KEYWORD="noip";  // 标题包含此关键词，激活noip模式，赛中不显示结果，仅保留最后一次提交。
static  $OJ_BEIAN=false;  // 如果有备案号，填写备案号

//static  $OJ_EXAM_CONTEST_ID=1000; // 启用考试状态，填写考试比赛ID
//static  $OJ_ON_SITE_CONTEST_ID=1000; //启用现场赛状态，填写现场赛比赛ID

/* share code */
static  $OJ_SHARE_CODE = false; // 代码分享功能
/* recent contest */
static  $OJ_RECENT_CONTEST = false;// "http://algcontest.rainng.com/contests.json" ; // 名校联赛

//$OJ_ON_SITE_TEAM_TOTAL用于根据比例的计算奖牌的队伍总数
//CCPC比赛的一种做法是比赛结束后导出终榜看AC至少1题的不打星的队伍数，现场修改此值即可正确计算奖牌
//0表示根据榜单上的出现的队伍总数计算(包含了AC0题的队伍和打星队伍)
static $OJ_ON_SITE_TEAM_TOTAL=0;

static $OJ_OPENID_PWD = '8a367fe87b1e406ea8e94d7d508dcf01';

/* weibo config here */
static  $OJ_WEIBO_AUTH=false;
static  $OJ_WEIBO_AKEY='1124518951';
static  $OJ_WEIBO_ASEC='df709a1253ef8878548920718085e84b';
static  $OJ_WEIBO_CBURL='http://192.168.0.108/JudgeOnline/login_weibo.php';

/* renren config here */
static  $OJ_RR_AUTH=false;
static  $OJ_RR_AKEY='d066ad780742404d85d0955ac05654df';
static  $OJ_RR_ASEC='c4d2988cf5c149fabf8098f32f9b49ed';
static  $OJ_RR_CBURL='http://192.168.0.108/JudgeOnline/login_renren.php';
/* qq config here */
static  $OJ_QQ_AUTH=false;
static  $OJ_QQ_AKEY='1124518951';
static  $OJ_QQ_ASEC='df709a1253ef8878548920718085e84b';
static  $OJ_QQ_CBURL='192.168.0.108';


//if(date('H')<5||date('H')>21||isset($_GET['dark'])) $OJ_CSS="dark.css";
if (isset($_SESSION[$OJ_NAME . '_' . 'OJ_LANG'])) {
	$OJ_LANG = $_SESSION[$OJ_NAME . '_' . 'OJ_LANG'];
} else if (isset($_COOKIE['lang']) && in_array($_COOKIE['lang'], array("uz", "en", 'ko'))) {
	$OJ_LANG = $_COOKIE['lang'];
} else if (isset($_GET['lang']) && in_array($_GET['lang'], array("uz", "en", 'ko'))) {
	$OJ_LANG = $_GET['lang'];
} else if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strstr($_SERVER['HTTP_ACCEPT_LANGUAGE'], "zh-CN")) {
	$OJ_LANG = "en";
}
/*
if (isset($_SESSION[$OJ_NAME . '_' . 'OJ_LANG'])) {
	$OJ_LANG = $_SESSION[$OJ_NAME . '_' . 'OJ_LANG'];
} else if (isset($_COOKIE['lang']) && in_array($_COOKIE['lang'], array("cn", "ug", "en", 'fa', 'ko', 'th'))) {
	$OJ_LANG = $_COOKIE['lang'];
} else if (isset($_GET['lang']) && in_array($_GET['lang'], array("cn", "ug", "en", 'fa', 'ko', 'th'))) {
	$OJ_LANG = $_GET['lang'];
} else if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strstr($_SERVER['HTTP_ACCEPT_LANGUAGE'], "zh-CN")) {
	$OJ_LANG = "cn";
}
*/

require_once(dirname(__FILE__)."/pdo.php");

		// use db
	//pdo_query("set names utf8");	
		


	//sychronize php and mysql server with timezone settings, dafault setting for China
	//if you are not from China, comment out these two lines or modify them.
	//date_default_timezone_set("PRC");
	//pdo_query("SET time_zone ='+8:00'");

