<?php
	//oj-header.php
	$MSG_FAQ="F.A.Qs";
	$MSG_BBS="DOSKA";
	$MSG_HOME="Bosh sahifa";
	$MSG_PROBLEMS="Masalalar";
	$MSG_STATUS="Urinishlar";
	$MSG_RANKLIST="Reytinglar";
	$MSG_CONTEST="Musobaqalar";
	$MSG_RECENT_CONTEST="So'ngi musobaqa";
	$MSG_LOGOUT="Chiqish";
	$MSG_LOGIN="Kirish";
	$MSG_LOST_PASSWORD="Parol unutdingizmi?";
	$MSG_REGISTER="Ro'yhatdan o'tish";
	$MSG_ADMIN="Administrator";
	$MSG_STANDING="Hozirgi holati";
	$MSG_STATISTICS="Natijalar";
	$MSG_USERINFO="Foydalanuvchi ma'lumotlari";
	$MSG_MAIL="Pochta manzili";
	//status.php

	$MSG_Pending="Navbatga qo`yildi ...";
	$MSG_Pending_Rejudging="Navbatda qayta tekshirilmoqda ...";
	$MSG_Compiling="Kompilyatsiya qilinmoqda";
	$MSG_Running_Judging="Ishga tushirilmoqda va tekshirilmoqda...";
	$MSG_Accepted="Qabul qilindi";
	$MSG_Presentation_Error="E`lon qilishda xatolik";
	$MSG_Wrong_Answer="Xato natija yuborilgan";
	$MSG_Time_Limit_Exceed="Belgilangan vaqtdan ortib ketdi";
	$MSG_Memory_Limit_Exceed="Belgilangan hajmdan ortib ketdi";
	$MSG_Output_Limit_Exceed="Chiqarish chegarasi ortib ketdi";
	$MSG_Runtime_Error="Ishga tushirish vaqtida xatolik";
	
	$MSG_TEST_RUN="Test ishga tushirildi";
	$MSG_Compile_Error="Kompilyatsiyada xatolik";
	$MSG_Runtime_Click="Ishga tushirish vaqtida xatolik";
	$MSG_Compile_Click="Kompilyatsiya qilishda xatolik";
	$MSG_Click_Detail="Batafsil ma`lumot olish";
	$MSG_Compile_OK="Kompilyatsiya muvaffaqiyatli yakunlandi";
	$MSG_RUNID="Nomer";
	$MSG_USER="Foydalanuvchi";
	$MSG_PROBLEM="Masala";
	$MSG_RESULT="Natija";
	$MSG_MEMORY="Hajm";
	$MSG_TIME="Vaqt";
	$MSG_LANG="Til";
	$MSG_CODE_LENGTH="Kod hajmi";
	$MSG_SUBMIT_TIME="Jo`natish vaqti";
	$MSG_Manual="Tekshirish";
   	$MSG_OK="Yakunlandi";
   	$MSG_Explain="Tushuntirish turi";
	//problemstatistics.php
	$MSG_PD="Navbatga qo`yildi";
	$MSG_PR="Qayta tekshirish uchun navbatga qo`yildi";
	$MSG_CI="Kompilyatsiya qilinmoqda";
	$MSG_RJ="Qayta tekshirilmoqda";
	$MSG_AC="Qabul qilindi";
	$MSG_PE="E`lon qilishda xatolik bor";
	$MSG_WA="Xato yechim";
	$MSG_TLE="Vaqt chegarasidan ortib ketdi";
	$MSG_MLE="Hajm chegarasidan ortib ketdi";
	$MSG_OLE="Chiqarish chegarasidan ortib ketdi";
	$MSG_RE="Ishga tushirish vaqtida xatolik ";
	$MSG_CE="Kompilyatsiyada xatolik";
	$MSG_CO="Kompilyatsiya muvaffaqiyatli tugadi";
	$MSG_TR="Test qilish yakunlandi";
	$MSG_RESET="Qayta ishga tushirish";
	//problemset.php
	$MSG_SEARCH="Qidiruv";
	$MSG_PROBLEM_ID="Masala nomeri";
	$MSG_TITLE="Masala sarlavhasi";
	$MSG_SOURCE="Ma`lumot/turi";
	$MSG_SUBMIT="Jo`natish";

	//ranklist.php
	$MSG_Number="Nomer";
	$MSG_NICK="Tahallus";
	$MSG_SOVLED="Yechilgan";
	$MSG_RATIO="Ko`rsatkich";
	//registerpage.php
	$MSG_USER_ID="Login";
	$MSG_PASSWORD="Parol";
	$MSG_REPEAT_PASSWORD="Qayta parolni kiriting";
	$MSG_SCHOOL="Maktab";
	$MSG_EMAIL="Elektron pochta";
	$MSG_REG_INFO="Ro`yhatga olingan ma'lumotlar";
	$MSG_VCODE="Tasdiqlanuchi kod";

	//problem.php
	$MSG_NO_SUCH_PROBLEM="Masalalar ruxsat etilmagan xolatda";
	$MSG_Description="Batafsil ma`lumot";
	$MSG_Input="Kiritish";
	$MSG_Output= "Chiqarish";
	$MSG_Sample_Input= "Kiritiladigan ma`lumotlar";
	$MSG_Sample_Output= "Chiqariladigan ma`lumotlar";
	$MSG_Test_Input= "Test uchun kiritish" ;
	$MSG_Test_Output= "Test uchun chiqarish" ;
	$MSG_SPJ= "Maxsus tekshirish" ;
	$MSG_HINT= "Hint";
	$MSG_Source= "Manba";
	$MSG_Time_Limit="Vaqt chegarasi";
	$MSG_Memory_Limit="Hajm chegarasi";
//admin menu
	$MSG_SEEOJ="Bosh sahifa";
	$MSG_ADD="Qo`shish ";
	$MSG_LIST="Ro`yhat ";
	$MSG_NEWS="Yangiliklar";
	$MSG_TEAMGENERATOR="Jamoa shakllantirish";
	$MSG_SETMESSAGE="Xabarlarni boshqarish";
	$MSG_SETPASSWORD="Parolni tahrirlash";
	$MSG_REJUDGE="Qayta tekshirish";
	$MSG_PRIVILEGE="Ruxsatlar";
	$MSG_GIVESOURCE="Manbalarni ulashish";
	$MSG_IMPORT="Bazaga yuklash";
	$MSG_EXPORT="Bazadan olish";
	$MSG_UPDATE_DATABASE="Bazani tahrirlash";
	$MSG_ONLINE="Online";
   	$MSG_SET_LOGIN_IP="LOGIN IP boshqarish ";

  $MSG_PRIVATE_WARNING="Musobaqa hali boshlanmadi yoki sizning ishtirok etish ruhsatingiz mavjud emas";
  $MSG_WATCH_RANK="Musobaqa reytingi";
  $MSG_NOIP_WARNING="Musobaqa yakunlanmaguncha natijalar xabar qilinmaydi";
  $MSG_Public="Ommaviy";
  $MSG_Private="Shaxsiy";
  $MSG_Running="Musobaqa";
  $MSG_Start="Boshlanadi";
  $MSG_End="Yakunlanadi";
  $MSG_TotalTime="Umumiy vaqt";
  $MSG_LeftTime="davomiyligi:";
  $MSG_Ended="Yakunlangan";
  $MSG_Login="Iltimos tizimga kiring";
  $MSG_JUDGER="Tekshiruvchi";

  $MSG_SOURCE_NOT_ALLOWED_FOR_EXAM="Musobaqadan oldin masalalarni olish mumkin emas";
  $MSG_BBS_NOT_ALLOWED_FOR_EXAM="Musobaqa jarayonida BBS dan foydalanish mumkin emas";
  $MSG_MODIFY_NOT_ALLOWED_FOR_EXAM="Musobaqa jarayonida parolingizni o'zgartirish mumkin emas";
  $MSG_MAIL_NOT_ALLOWED_FOR_EXAM="Musobaqa jarayonida elektron pochtadan foydalanish mumkin emas";
  $MSG_LOAD_TEMPLATE_CONFIRM="Tizimning ko`rinishini o`zgartirmoqchimisz? Hamma kodlar va o`zgarishlar o`chirib yuboriladi";
  $MSG_BLOCKLY_OPEN="Bloklangan";
  $MSG_BLOCKLY_TEST="Bloklangan testni ishga tushirish";
  $MSG_MY_SUBMISSIONS="Mening urinishlarim";
  $MSG_MY_CONTESTS="Mening musobaqalarim";
  $MSG_Creator="Muallif";
  $MSG_IMPORTED="Yuklab olingan";
  $MSG_USER="Foydalanuvchi";
  $MSG_PRINTER="Qo`g`ozga nuxsa olish";
  $MSG_PRINT_DONE="Qo`g`ozga nuxsa olish yakunlandi";
  $MSG_PRINT_PENDING="Qo`g`ozga nuxsa olish navbatga qo`yildi";
  $MSG_PRINT_WAITING="Qo`g`ozga nuxsa olish jarayonda, iltimos kuting";
  $MSG_COLOR="Color";
  $MSG_BALLOON="Balloon"; 	
  $MSG_BALLOON_DONE="Balloon Sent";
  $MSG_BALLOON_PENDING="Balloon Pending";
  $MSG_HELP_SEEOJ="Bosh sahifaga qaytish";
  $MSG_HELP_ADD_NEWS="Yangilik qo`shish";
  $MSG_HELP_NEWS_LIST="Yangiliklarni tahrirlash";
  $MSG_HELP_USER_LIST="Ruxsatli/Ruxsatsiz foydalanuvchi";


  $MSG_HELP_ADD_PROBLEM="Yangi masala yaratish";
  $MSG_HELP_PROBLEM_LIST="Masalalarni tahrirlash";
  $MSG_HELP_ADD_CONTEST="Yangi musobaqa yaratish";
  $MSG_HELP_CONTEST_LIST="Musobaqalar";
  $MSG_HELP_TEAMGENERATOR="Gruppalar yaratish";
  $MSG_HELP_SETMESSAGE="Xabarlarni boshqarish";
  $MSG_HELP_SETPASSWORD="Parollarni boshqarish";
  $MSG_HELP_REJUDGE="Qayta tekshirish";
   $MSG_HELP_ADD_PRIVILEGE="Ruxsatlarni boshqarish";
    $MSG_HELP_PRIVILEGE_LIST= "Mavjud ruxsatlar";
    $MSG_HELP_GIVESOURCE="Manbalarni ulashish";
    $MSG_HELP_EXPORT_PROBLEM="Tizimdan masalalarni yuklab olish";
    $MSG_HELP_IMPORT_PROBLEM="Tizimga masalalarni yuklash";
    $MSG_HELP_UPDATE_DATABASE= "Tizimning bazasini o`zgartirish";
    
$MSG_HELP_ONLINE= "Online foydalanuvchilar";
  $MSG_HELP_PE="Chiqariladigan ma`lumotlar tizimdagi bilan bir xil emas";
  $MSG_HELP_WA="Xato natija yuborilgan";
  $MSG_HELP_TLE="Belgilangan vaqt chegarasidan ortib ketmoqda";
  $MSG_HELP_MLE="Belgilangan hajm chegarasidan ortib ketmoqda ";
  $MSG_HELP_OLE="Chiqariladigan ma`lumotlar chegaradan ortib ketmoqda";
  $MSG_HELP_RE="Kompilyatsiya qilishda vaqt chegarasidan ortib ketmoqda";
  $MSG_HELP_CE="Kompilyatsiya qilishda xatolik bor";
  $MSG_HELP_AC="Qabul qilindi";
  
$MSG_HELP_MORE_TESTDATA_LATER="Masala kiritilgandan keyin ko`proq test manbalarini kiritish mumkin";
$MSG_HELP_SPJ="mARCUZ TEST UCHUN";
  $MSG_HELP_BALLOON_SCHOOL="Maktab ma`lumotlari";
  $MSG_WARNING_LOGIN_FROM_DIFF_IP="Boshqa Ip dan kirish";
  $MSG_WARNING_DURING_EXAM_NOT_ALLOWED=" musobaqa jarayonida mumkin emas";
  $MSG_WARNING_ACCESS_DENIED="Kechirasiz, siz bu ma`lumotni ko`ra olmaysiz chunki u sizga tegishli emas yoki administrator bunga ruxsat bermaydi";
  $MSG_LOSTPASSWORD_MAILBOX="Elektron manzilingizga yuborilgan kodni kiriting:";
  $MSG_LOSTPASSWORD_WILLBENEW="Agar siz kiritgan kod mos kelsa, sizga yangi parol bo`ladi";
?>

